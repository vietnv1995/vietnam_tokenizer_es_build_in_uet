package org.apache.lucene.analysis.vi;

import org.elasticsearch.SpecialPermission;
import vn.edu.vnu.uet.nlp.segmenter.UETSegmenter;
import vn.hus.nlp.tokenizer.ITokenizerListener;
import vn.hus.nlp.tokenizer.ResultMerger;
import vn.hus.nlp.tokenizer.ResultSplitter;
import vn.hus.nlp.tokenizer.Tokenizer;
import vn.hus.nlp.tokenizer.io.Outputer;
import vn.hus.nlp.tokenizer.segmenter.AbstractResolver;
import vn.hus.nlp.tokenizer.tokens.LexerRule;
import vn.hus.nlp.tokenizer.tokens.TaggedWord;
import vn.hus.nlp.tokenizer.tokens.WordToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.Reader;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TokenizerImp {

    private LexerRule[] rules = new LexerRule[0];
    private InputStream inputStream;
    private LineNumberReader lineReader;
    private String line;
    private int column;
    private List<TaggedWord> result = null;
    private Outputer outputer = null;
    private final List<ITokenizerListener> tokenizerListener = new ArrayList();
    private UETSegmenter segmenter;
    private boolean isAmbiguitiesResolved = true;
    private Logger logger;
    private final ResultMerger resultMerger;
//    private final ResultSplitter resultSplitter;
    private AbstractResolver resolver;

    public TokenizerImp(UETSegmenter segmenter) {
        this.result = new ArrayList();
        this.createOutputer();
        this.segmenter = segmenter;
        this.resultMerger = new ResultMerger();
//        this.resultSplitter = new ResultSplitter();
    }

    private void createOutputer() {
        if (this.outputer == null) {
            this.outputer = new Outputer();
        }

    }

    public void tokenize(Reader reader) throws IOException {
        this.result.clear();
        this.lineReader = new LineNumberReader(reader);
        this.line = null;
        this.column = 1;
        while(true) {
            TaggedWord taggedWord = this.getNextToken();
            if (taggedWord == null) {
                if (this.lineReader != null) {
                    this.lineReader.close();
                }

                this.result = this.resultMerger.mergeList(this.result);
                return;
            }

//            if (!taggedWord.isPhrase()) {
//                if (taggedWord.isNamedEntity()) {
//                    TaggedWord[] tokens = this.resultSplitter.split(taggedWord);
//                    if (tokens != null) {
//                        TaggedWord[] var10 = tokens;
//                        int var11 = tokens.length;
//
//                        for(int var12 = 0; var12 < var11; ++var12) {
//                            TaggedWord token = var10[var12];
//                            this.result.add(token);
//                        }
//                    } else {
//                        this.result.add(taggedWord);
//                    }
//                } else if (taggedWord.getText().trim().length() > 0) {
//                    this.result.add(taggedWord);
//                }
//            } else {
                String phrase = taggedWord.getText().trim();
                if (!this.isSimplePhrase(phrase)) {
                    String ruleName = taggedWord.getRule().getName();
                    String[] tokens = null;
                    List<String[]> segmentations = mySegment(phrase, this.segmenter);
                    if (segmentations.size() == 0) {
                        this.logger.log(Level.WARNING, "The segmenter cannot segment the phrase \"" + phrase + "\"");
                    }

                    if (this.isAmbiguitiesResolved() && segmentations.size() > 1) {
                        tokens = this.resolver.resolve(segmentations);
                    } else {
                        Iterator<String[]> it = segmentations.iterator();
                        if (it.hasNext()) {
                            tokens = (String[])it.next();
                        }
                    }

                    if (tokens == null) {
                        this.logger.log(Level.WARNING, "Problem: " + phrase);
                    }

                    for(int j = 0; j < tokens.length; ++j) {
                        WordToken token = new WordToken(new LexerRule(ruleName), tokens[j], this.lineReader.getLineNumber(), this.column);
                        this.result.add(token);
                        this.column += tokens[j].length();
                    }
                } else if (phrase.length() > 0) {
                    this.result.add(taggedWord);
                }
//            }

            this.fireProcess(taggedWord);
        }
    }

    public List<TaggedWord> mySegmenter(final Reader r) throws IOException {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            // unprivileged code such as scripts do not have SpecialPermission
            securityManager.checkPermission(new SpecialPermission());
        }

        List<TaggedWord> result = new ArrayList<>();
        char[] arr = new char[8 * 1024];
        StringBuilder buffer = new StringBuilder();
        int numCharsRead;
        while ((numCharsRead = r.read(arr, 0, arr.length)) != -1) {
            buffer.append(arr, 0, numCharsRead);
        }
        r.close();
        final String targetString = buffer.toString();
        String segRes = AccessController.doPrivileged(new PrivilegedAction<String>() {
            @Override
            public String run() {
                return segmenter.segment(targetString);
            }
        });
        String[] splited = segRes.split("\\s+");
        for(int i = 0; i < splited.length; i++){
            LexerRule rule = new LexerRule("token");
            TaggedWord tag = new TaggedWord(rule, splited[i]);
            result.add(tag);
        }
        return result;
    }

    public List<String[]> mySegment(String phrase, UETSegmenter segmenter) {
        List<String[]> res = new ArrayList<>();
        String[] r = new String[]{"Xin chao", "Viet Nam"};
        res.add(r);
        return res;
    }

    public void setAmbiguitiesResolved(boolean b) {
        this.isAmbiguitiesResolved = b;
    }

    public boolean isAmbiguitiesResolved() {
        return this.isAmbiguitiesResolved;
    }

    public List<TaggedWord> getResult() {
        return this.result;
    }


    private boolean isSimplePhrase(String phrase) {
        phrase = phrase.trim();
        return phrase.indexOf(32) < 0;
    }

    private TaggedWord getNextToken() throws IOException {
        if (this.line == null || this.line.length() == 0) {
            this.line = this.lineReader.readLine();
            if (this.line == null) {
                if (this.inputStream != null) {
                    this.inputStream.close();
                }

                this.lineReader = null;
                return null;
            }

            if (this.line.trim().length() == 0) {
                System.err.println("Create an empty line tagged word...");
                return new TaggedWord(new LexerRule("return"), "\n");
            }

            this.column = 1;
        }

        TaggedWord token = null;
        int tokenEnd = -1;
        int longestMatchLen = -1;
        int lineNumber = this.lineReader.getLineNumber();
        LexerRule selectedRule = null;

        int endIndex;
        for(endIndex = 0; endIndex < this.rules.length; ++endIndex) {
            LexerRule rule = this.rules[endIndex];
            Pattern pattern = rule.getPattern();
            Matcher matcher = pattern.matcher(this.line);
            if (matcher.lookingAt()) {
                int matchLen = matcher.end();
                if (matchLen > longestMatchLen) {
                    longestMatchLen = matchLen;
                    tokenEnd = matchLen;
                    selectedRule = rule;
                }
            }
        }

        endIndex = tokenEnd;
        if (tokenEnd < this.line.length() && this.line.charAt(tokenEnd) == '@') {
            while(endIndex > 0 && this.line.charAt(endIndex) != ' ') {
                --endIndex;
            }
        }

        if (endIndex == 0) {
            endIndex = tokenEnd;
        }

        if (selectedRule == null) {
            selectedRule = new LexerRule("word");
        }

        String text = this.line.substring(0, endIndex);
        token = new TaggedWord(selectedRule, text, lineNumber, this.column);
        this.column += endIndex;
        this.line = this.line.substring(endIndex).trim();
        return token;
    }

    private void fireProcess(TaggedWord token) {
        Iterator var2 = this.tokenizerListener.iterator();

        while(var2.hasNext()) {
            ITokenizerListener listener = (ITokenizerListener)var2.next();
            listener.processToken(token);
        }

    }
}
