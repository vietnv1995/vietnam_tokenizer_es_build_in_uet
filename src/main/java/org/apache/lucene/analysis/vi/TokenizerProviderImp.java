package org.apache.lucene.analysis.vi;

import vn.edu.vnu.uet.nlp.segmenter.UETSegmenter;

public class TokenizerProviderImp {

    private static TokenizerProviderImp provider = null;
    private TokenizerImp tokenizer;
    private UETSegmenter segmenter;

    private TokenizerProviderImp(){
        this.segmenter = new UETSegmenter("models");
        tokenizer = new TokenizerImp(this.segmenter);
    }

    public static TokenizerProviderImp getInstance() {
        if (provider == null) {
            provider = new TokenizerProviderImp();
        }

        return provider;
    }

    public TokenizerImp getTokenizer() {
        return this.tokenizer;
    }
}
